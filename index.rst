Welcome to the 2KLIC Platform documentation!
============================================

.. warning:: Under construction! Some sections may be out of date with
   respect to the current public API!

Contents:

.. toctree::
   :maxdepth: 2

   overview/index
   tutorial/index
   api/index
   models/index
   alarm-system-glossary

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
