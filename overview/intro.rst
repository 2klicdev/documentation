Introduction
============

The 2KLIC Platform has three foundational parts: a set of device and
location hierarchies; alarm systems; and a user access control system
that strictly controls permissions to all parts of the platform. Each
part of the platform can be observed for changes via events, if
permitted by user access control.

Devices and Locations
---------------------

.. index:: device, capability

A device corresponds one-to-one with a physical device, and has a set
of capabilities that each correspond to a particular sensor's readings
or a controllable property. For example, a thermostat will have a
sensor reading of the current air temperature, as well as one or more
controllable setpoints (the desired temperature) and mode settings; a
light switch will have an on/off switch or dimmer control. Some device
types, for example Smart Controllers, have a hierarchical tree of
sub-devices, each of which may manage their own
sub-devices. Sub-devices may or may not correspond to separate
physical devices: a power strip may have individual sub-devices for
each outlet.

.. index:: location

Locations are user-defined places, and may correspond to physical
locations as large as cities or apartment buildings and complexes,
down to individual apartments and rooms within those locations. The
location trees are complementary to and overlap the device trees. The
root of each device tree is a single Smart Controller, and the root of
each location tree is a :index:`property`, ....

.. TODO briefly describe root locations

Alarm Systems
-------------

.. index:: alarm system, zone, sector

An alarm system is composed of zones and sectors. A zone corresponds
to a single sensor – a particular capability of a particular device –
and a sector is a collection of zones that can be armed or disarmed as
a group. When an armed zone detects an abnormal state (e.g. smoke,
battery depleted) an alarm event is generated, and the zone remains in
an alarm state until disarmed. Not all applications require alarm
systems; however, each Smart Controller is configured with a single
alarm system by default.

User Access Control
-------------------

.. TODO summarize user/group access system

Events
------

.. index:: event

Changes detected by the Smart Controller generate events that API
clients can subscribe to. Each event corresponds to a resource that
can also be explicitly queried by its path in the API. The events that
can be observed by a particular client are limited by the permissions
that client has according to user access control.
