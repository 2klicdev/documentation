.. _ZWaveModelData:

Z-Wave Model Data
=================

Device models that specify the ``"zwave"`` protocol may contain
additional information for the Smart Controller to handle
device-specific behavior.  In this case, the ``protocolInfo`` field of
the device model must be an object with optional fields described
below.  A knowledge of Z-Wave terminology is assumed; a description of
the terminology related to Z-Wave commands and networking is beyond
the scope of this document.

``associations``
   An array of association group numbers that the controller should
   associate itself with when the device is added to the network.
   Z-Wave devices supporting association are *always* set to associate
   the Smart Controller with group one, so only additional groups need
   to be specified here.

``basic``
   An object describing how the Basic command class maps to a
   capability.  This is only necessary if the device does not provide
   the necessary functionality via a command class that maps directly
   to the capability, for instance some sensors may only send a Basic
   Set to associated nodes when their state changes.

   ``capability``
      The name of the capability that basic commands map to.

   ``values``
      An object mapping Z-Wave basic values to capability values.  The
      key ``_`` may be used as a catch-all, for example:

      .. code-block:: javascript

         {
            "capability": "waterLeak",
            "values": {
               "0": "idle",
               "_": "detected"
            }
         }

      When the Basic command class maps to a writable capability,
      exact values must be provided for each capability value in order
      to send the correct Basic Set command.

``quirks``
   An object describing capability behaviour.  Each key must be the
   name of a capability, and each value an object with a set of
   quirks.

   ``reset``
      An object describing how this capability should be reset to a
      specific value after an interval.  This may be necessary for
      alarm sensors that do not send a "clear" event.

      ``value``
         The value to reset to.
      ``interval``
         The number of seconds after which to reset the capability
         value.  The default (and minimum) value is 30 seconds.  The
         capability *is not* guaranteed to be reset after exactly this
         interval.  The capability *is* guaranteed to not be reset
         until after this interval has expired.  The reset *should*
         happen within 30 seconds of the interval expiration.

   ``poll``
      An object (or ``true``) describing how to poll for this
      capability.  Polling should only be used if there is no other
      way to get the device to send its updated values: reports sent
      via associations must be preferred.

      ``interval``
        The number of seconds between polling.  The default (and
        minimum) value is 10 seconds.  Polling *is not* guaranteed to
        happen on this interval, as there are requirements prescribed
        by the Z-Wave specification regarding polling.  Polling *is*
        guaranteed to happen *no more often* than this interval.

   .. code-block:: javascript

      { // protocolInfo object.
         "quirks": {
            "alarm": { // The capabilityId.
               "reset": { // Reset to "idle" after at least 30 seconds.
                  "value": "idle"
               }
            },
            "switch": { // The capabilityId.
               "poll": true // Poll, no more often than every 10 seconds.
            }
         }
      }
