.. _ModelFields:

Generic Fields
==============

There are several fields in the device model that apply to all device
models.

``_id``
   The unique identifier for this device model.

``name``
   The canonical product name according to its manufacturer.

``label``
   An object with localized versions of the product name.  Each key is
   the language identifier (e.g. ``en``, ``fr``, ``es``, etc).

``manufacturer`` (if known)
   The manufacturer of the device.

``type``
   A general device type to describe this device.  A complete list of
   device types is forthcoming.  Examples include ``thermostat`` and
   ``binary switch``.

``protocolId``
   Identifies the device protocol handler used within the Smart
   Controller to communicate with the device.  Examples include
   ``zwave`` and ``retrofit`` (TBD, currently called ``iom``).

``capabilities``
   Metadata describing the values of the device's capabilities.
   Described further in the :ref:`capabilities section
   <CapabilityModels>`.

``parameters``
   Metadata describing the values of the device's parameters.
   Described further in the :ref:`parameters section
   <ParameterModels>`.

``requireHub``
   A boolean indicating whether this is a device managed directly by a
   Smart Controller.  If false, the device is a virtual device managed
   by the Platform API.

``published``
   A boolean indicating whether this model should be displayed to the
   user in user interfaces when selecting a device to add.
