.. _ZWaveSensorScales:

Z-Wave Sensor Scales
====================

This is a non-prescriptive description of the sensor scales
(capability units) used by Z-Wave multilevel sensor devices and the
2KLIC Smart Controller as of July 2016.

.. literalinclude:: zwave_sensor_scales.json
   :language: javascript
