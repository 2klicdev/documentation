.. _CapabilityModels:

Capability Models
=================

The ``capabilities`` field of the device model must be an array of
capability models.  Each capability model has a ``capabilityId``, a
``type``, a set of supported ``methods``, and some additional
properties that depend on the ``type``.  The ``capabilityId`` is the
logical name of the capability and is used to refer to it in relation
to the device.  The ``methods`` field must be an array of one or two
items: ``["get"]`` for read-only capabilities, and ``["get", "put"]``
(in either order) for read-write capabilities (more methods may be
added in the future).

Because of the relation between the logical name of capabilities (the
``capabilityid``) and devices, a device must only have one capability
with a given logical name.  Devices that have multiple capabilities of
the same name must be split up into sub-devices.  For example, a power
strip may have a master parent device controlling the power of all
outlets with a "switch" capability, and sub-devices for individual
control of each outlet with "switch" capabilities of their own.

.. _EnumCapabilityModels:

Enum Capabilities
-----------------

The ``"enum"`` capability type describes a fixed set of enumerated
values represented as strings.  The possible values are enumerated in
the ``range`` property of the capability model, which must be an array
of strings.  If there is a sensible default value ("off" or "idle"),
it *should* be in the first position of the array.

.. code-block:: javascript

   {
      "capabilityId": "switch",
      "type": "enum",
      "methods": ["get", "put"],
      "range": ["off", "on"]
   }

.. _FloatCapabilityModels:

Float Capabilities
------------------

The ``"float"`` capability type describes a scalar value.  The units
for the value must be listed in the ``units`` property of the
capability model as an array of strings, using standard unit
abbreviations.  A non-canonical list of sensor units can be found in
the :ref:`ZWaveSensorScales` section.  If applicable/known, the range
of values must be described by the ``range`` property, which must be
an array of two elements: the minimum and the maximum, in that order,
in the same format as a capability value described by this model.

.. code-block:: javascript

   {
      "capabilityId": "heatingSetpoint",
      "type": "float",
      "methods": ["get", "put"],
      "units": ["C", "F"],
      "range": [
         { "value": 5, "unit": "C" },
         { "value": 30, "unit": "C" }
      ]
   }

.. _RgbCapabilityModels:

RGB Capabilities
----------------

The ``"rgb"`` capability type describes a color control.  It has no
additional properties.  Values of this capability are represented as a
string containing a hexadecimal RGB triplet prefixed by a hash:
``"#ABCDEF"``.  Other color formats may be specified in the future.

.. code-block:: javascript

   {
      "capabilityId": "color",
      "type": "rgb",
      "methods": ["get", "put"]
   }

.. _UriCapabilities:

URI Capabilities
----------------

The ``"uri"`` capability type describes a URI.  It has no additional
properties.  Values of this capability are represented as a string
containing the URI.

.. code-block:: javascript

   {
      "capabilityId": "stream",
      "type": "uri",
      "methods": ["get"]
   }
