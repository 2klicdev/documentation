.. _ParameterModels:

Parameter Models
================

The ``parameters`` field of the device model must be an array of
parameter models.  Each parameter model has a ``parameter`` (its name)
and a default ``value``.  Parameter models will be further elaborated
with types and metadata similar to capability models in a subsequent
version of the API.
