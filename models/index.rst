Device Model Reference
======================

Contents:

.. toctree::
   :maxdepth: 2

   general
   capabilities
   parameters
   zwave
   zwave_sensor_scales

This reference guide describes the structure of device model objects
in detail.  The generic sections will be helpful to both model authors
and API users: :ref:`ModelFields`, :ref:`CapabilityModels`, and
:ref:`ParameterModels`.  The internals sections will be most helpful
to model authors, and for customizing models to deal with device
quirks: :ref:`ZWaveModelData`.

The key words *must*, *must not*, *required*, *shall*, *shall not*,
*should*, *should not*, *recommended*, *may*, and *optional* in this
document are to be interpreted as described in `RFC 2119`_.

.. _`RFC 2119`: https://tools.ietf.org/html/rfc2119
