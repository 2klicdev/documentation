Alarm System Glossary
=====================

.. glossary::
   
   24-hour zone

      A zone that cannot be disarmed. Typical examples are smoke
      detectors and medical alert/panic buttons.

   bypass

      A zone may be bypassed instead of armed. In this case, the state
      of the sensor is ignored. 24-hour zones cannot be bypassed (they
      may be suspended). Tripped and troubled zones cannot be
      armed—they must be returned to a normal state before the system
      can arm. Tripped and troubled zones can, however, be bypassed in
      order to arm the system anyway. The bypassed state is cleared
      when the zone is disarmed (by disarming the system or all
      sectors that the zone belongs to).

   entry delay

      An optional user-specified delay to wait after an armed zone is
      tripped before raising an alarm. This is for example used to
      give the homeowner a chance to disarm the system after opening
      the front door.

   exit delay

      An optional user-specified delay to wait after arming the
      system/sector/perimeter before arming a zone. This is for
      example used to give the homeowner a chance to exit the front
      door after arming the system.

   perimeter
   night arm

      A user-defined collection of zones, meant to be
      intrusion-detection zones on the perimeter of the alarm
      system—typically armed at night when the homeowner is at home,
      and so the action of arming the perimeter is referred to as
      "night arm". The perimeter is similar to a sector, but cannot be
      armed or disarmed individually along with other sectors.

   PIN

      A string of digits used to authorize a user's access to the
      alarm system. At the API level, a PIN must be validated to
      receive a limited-time single-use token that must be supplied
      when sending an arm/disarm command to the alarm system.  The PIN
      entry field in a UI should be cleared after validating a PIN;
      from then the user has some time (60 seconds) to complete any
      other necessary arming steps. Failure to complete the action in
      time should be indicated to the user, and PIN re-entry required.

   sector

      A user-defined collection of zones. Sectors may be armed and
      disarmed individually. In the case of zones in overlapping
      sectors, the 'armed' state takes precedence if one sector is
      armed and the other(s) disarmed.

   suspend

      24-hour zones may not be bypassed, but they can be suspended for
      up to 8 hours, if and only if the system has been completely
      disarmed (e.g. while the homeowner is on the premises, for
      example to suppress a smoke alarm while cooking).

   tripped zone

      A zone is tripped if the capability it corresponds to is not in
      the zone's 'normal' state. A tripped state raises an alarm if
      the zone is armed.

   troubled zone

      A zone is troubled if the device it corresponds to has failed
      (battery empty, line cut, etc). A troubled state raises an alarm
      if the zone is not disarmed or bypassed.

   zone

      A sensor that can be armed to trigger alarms. A zone corresponds
      to a single capability of a particular device. While armed, if
      the capability leaves a zone-specific 'normal' state (if the
      zone is tripped), an alarm is raised.
