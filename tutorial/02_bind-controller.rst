Binding a Smart Controller
==========================

.. _sdk-helper:

.. note:: From now on we will abstract the SDK initialization into a
   separate module, to avoid repeating ourselves:

   .. literalinclude:: sdk.js
      :language: javascript

.. _controller-binding-tutorial:

In order to communicate with your Smart Controller (either a real one
or a virtual sandbox controller), it must be bound to your user
account. You will need the MAC address of your controller for this
step. It should be set as the ``controllerMac`` property of the config
module.

.. literalinclude:: 02_bind-controller.js
   :language: javascript

Running this script, you should receive output similar to this:

.. _controller-binding-result:

.. literalinclude:: 02_output.js
   :language: javascript

We see a few things about the controller in the ``data`` object:

.. _gateway-id-binding:

``_id``
   The controller ID. Save this as the ``gatewayId`` in your config
   module, we'll need it later in the :ref:`devices tutorial
   <devices-tutorial>`.

``createdTs``
   The timestamp when the controller was provisioned.

``lastPing``
   The timestamp at which the controller last sent a ping to the API
   server.

``latency``
   The amount of time, in milliseconds, that the previous ping took
   (the roundtrip time from the controller to the API server).

.. error:: You might instead see this error:

   .. literalinclude:: 02_error.js
      :language: javascript

   This indicates that no controller with the specified MAC address
   has provisioned itself with the API server. Verify the
   ``controllerMac`` in your config and ensure your Smart Controller
   is powered on and connected to the internet. If it still does not
   work, `contact support`_.

   .. _`contact support`: support@developer.2klic.com

   Another error you might encounter, for example by running this
   tutorial's code a second time, is:

   .. literalinclude:: 02_already_bound.js
      :language: javascript

   This indicates that a user has already bound the controller with
   the specified MAC address.
