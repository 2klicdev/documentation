Devices
=======

.. _devices-tutorial:

.. index:: device

Most devices are sub-devices of a Smart Controller, so before
beginning this tutorial, you will need to have completed the
:ref:`controller binding tutorial <controller-binding-tutorial>` and
:ref:`saved the controller ID <gateway-id-binding>` as the
``gatewayId`` in your config module.

For the purposes of this tutorial, we will assume you are working with
a sandbox controller, which allows us to emulate physical devices in a
simple way. The emulator is not available on production controllers,
so if you are working with a physical Smart Controller you will need
some devices to test with.

Device Models
-------------

Each device has a corresponding model that describes the product's
name, manufacturer, required configuration parameters, and
capabilities. Let's take a look at an example:

.. code-block:: bash

   curl -fsSL https://api.2klic.io/v2/zwave_86_103_4b | json_pp

.. code-block:: json

   {
      "parameters" : [],
      "capabilities" : [
         {
            "capabilityId" : "switch",
            "_id" : "573a06b418c22ca1008312d0",
            "methods" : [
               "get",
               "put"
            ],
            "type" : "enum",
            "range" : [
               "off",
               "on"
            ],
            "units" : []
         }
      ],
      "protocolId" : "zwave",
      "type" : "binary switch",
      "createdTs" : "2016-05-16T17:43:16.645Z",
      "_id" : "zwave_86_103_4b",
      "requireHub" : true,
      "gui" : {
         "style" : "h1w1"
      },
      "manufacturer" : "AEON Labs",
      "name" : "Smart Switch Gen5"
   }

The fields we are interested in are:

``_id``
   The model ID. We will need this when we add the device.

``name``
   The product's name.

``manufacturer``
   The product manufacturer's name.

``type``
   A generic device type. The supported types will be detailed in
   forthcoming documentation.

``parameters``
   The device configuration parameters. This model has no
   configuration parameters.

``capabilities``
   The device's capabilities. Here we see one capability, a switch:
   ``"capabilityId": "switch"``; that can be observed and changed:
   ``"methods": ["get", "put"]``; that has two possible values:
   ``"type": "enum"`` and ``"range": ["off", "on"]``.

Adding a Device
---------------

Adding a new device is pretty simple once you have your Smart
Controller ID and device model.

.. sidebar:: Emulated and Z-Wave Devices

   We are using a sandbox controller, which allows us to create
   emulated devices by specifying ``isEmulated``.  On a real
   Smart Controller, this parameter should be omitted.

   This example uses a specific Z-Wave device model ID; for real
   Z-Wave devices, the ``generic_z_wave_device`` model ID should
   always be used.  The controller will query the device and return
   the correct model in the result.

.. literalinclude:: 04_01_create-device.js
   :language: javascript

Running this example should result in output similar to the following:

.. literalinclude:: 04_01_output.js
   :language: javascript

Take a look at the ``caps`` property of the resulting device data. We
can see that the emulated switch is currently off, and the timestamp
of when the capability was last updated.  Save the device's ``_id`` in
the config module as ``switchDevice`` for convenient access later in
the tutorial.

You can easily list all devices your logged in user account has
permission to view.

.. code-block:: javascript

   var sdk = require('./sdk');
   sdk.devices.list().then(console.dir);

Capabilities
------------

.. index:: capability

A capability (cap for short) describes some feature of a device, most
often sensor readings or controllable settings like on/off switches
and dimmer controls.  Each capability is described by a corresponding
object in the device model.

Capability Models
~~~~~~~~~~~~~~~~~

A device model may contain any number (but usually a small number) of
capability definitions, referred to as capability models.  Every
device will have exactly one cap for each capability definition in its
model. The name of each cap (the ``capabilityId`` in the cap model) is
unique per-device, and describes the device property it corresponds
to, for example: ``switch``, ``airTemperature``, ``humidity``, and
``motion``.

A capability may be read-only, or read-write. This is specified by the
``methods`` property of the cap model: ``["get"]`` for read-only caps,
and ``["get", "put"]`` for read-write caps.  More methods may be added
in the future.

There are four types of capabilities.  The type is specified by the
``type`` property of the capability model:

``enum``
   A fixed set of enumerated values, represented as strings.  The
   possible values are enumerated in the ``range`` property of the cap
   model, usually with a default ("off" or "idle") value in the first
   position of the range array.

``float``
   A numerical value, with a required unit (e.g. Celsius, percentage).
   Usually but not always with a minimum and maximum range.  The
   supported units are listed in the ``units`` property of the cap
   model.  The minimum and maximum values, if present, are found in
   the ``range`` property of the cap model, in that order.

``rgb``
   An RGB triplet, represented as a string with a leading hash and six
   hexadecimal digits (the common format found in stylesheets and
   elsewhere).  The cap model has no additional properties.

``uri``
   A URI.  The cap model has no additional properties.

Reading a Capability
~~~~~~~~~~~~~~~~~~~~

One can read a specific capability of a device:

.. literalinclude:: 04_02_read-cap.js
   :language: javascript

.. literalinclude:: 04_02_output.js
   :language: javascript

Or all of the capabilities of a device:

.. literalinclude:: 04_03_list-cap.js
   :language: javascript

.. literalinclude:: 04_03_output.js
   :language: javascript

Setting a Capability
~~~~~~~~~~~~~~~~~~~~

Setting the value of a read-write capability is equally simple:

.. literalinclude:: 04_04_set-cap.js
   :language: javascript

.. literalinclude:: 04_04_output.js
   :language: javascript

Emulating Capability Changes
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

When testing with a sandbox controller, it can be useful to emulate
capability changes for read-only capabilities, or in general
device-initiated changes of any caps whether they are read-only or
read-write.

.. literalinclude:: 04_05_emu-cap.js
   :language: javascript

.. literalinclude:: 04_05_output.js
   :language: javascript

This functionality could easily be incorporated into scripted
sequences for testing purposes.

.. Parameters
.. ----------

.. TODO: the use of parameters isn't even very well defined for us in
   our own usage yet. The only protocol to use them regularly is the
   IOM protocol, and we want to change it to have a set device tree
   with one device for the IOM board itself, and one sub-device per
   input and output; leaving only the input normal (NC or NO)
   parameter.

Removing Devices
----------------

Devices can be removed.

.. literalinclude:: 04_06_delete-device.js
   :language: javascript

.. literalinclude:: 04_06_output.js
   :language: javascript

.. caution:: Under some circumstances the Smart Controller may refuse
   to remove devices.  In particular, normally functioning Z-Wave
   devices cannot be removed in this manner, because of the way the
   protocol works.  Z-Wave device removal is initiated with another
   method, which waits for the user to complete removal on the device
   to be removed, or times out after 60 seconds.

   .. literalinclude:: 04_07_zwave-remove.js
      :language: javascript

   More details about the Z-Wave-specific aspects of the API will be
   available in a forthcoming addendum to our developer documentation.
