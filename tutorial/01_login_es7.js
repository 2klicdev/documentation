const config = require('./config');
const PlatformSdk = require('@2klic/2klic_io-sdk');

const sdk = new PlatformSdk({
    app_key: config.appKey,
    url: config.apiUrl,
    unsafe: config.unsafe,
    apiVersion: '2.0',
});

async function main() {
    try {
        const user = await sdk.authenticate({
            username: config.username,
            password: config.password,
        });
        console.log(user);
    } catch (error) {
        console.error(error);
    }
}

if (require.main === module) {
    main();
}
