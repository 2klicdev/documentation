var config = require('./config');
var sdk = require('./sdk');

function main() {
    return sdk.user.bindDevice(config.controllerMac).then(function (result) {
        console.log(result);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
