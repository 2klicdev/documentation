var config = require('./config');
var sdk = require('./sdk');

function main() {
    return sdk.devices.getCapability(
        config.switchDevice, 'switch'
    ).then(function (result) {
        console.dir(result.data);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
