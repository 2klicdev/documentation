# Adding a Device

To add a device, you will need to know the model ID of the device you
want to add. Let's search the production API database for a door
contact.

```javascript
var url = 'https://api.2klic.io/models?model=door+contact';
request(url, function (error, response, body) {
    console.log(JSON.parse(body));
});
```

```javascript
[ { _id: 55313db5874b78012a094ae1,
    model: Retrofit Door Contact,
    protocolId: iom,
    __v: 0,
    createdTs: 2015-06-16T16:42:59.438Z,
    parameters: [ [Object], [Object] ],
    capabilities: [ [Object] ],
    requireHub: true } ]
```

Ok, cool. We won't worry too much about the model object just
yet. Let's grab the `_id`, and create a device!

```javascript
request({
    method: 'POST',
    url: config.baseUrl + '/devices',
    json: {
        model: '55313db5874b78012a094ae1',
        name: 'My First Device',
        isEmulated: true
    }
}, function (error, response, body) {
    console.log(body);
});
```

The output should look something like the following:

```javascript
{ _id: '560ed06a6939c601c50b7438',
  model: '55313db5874b78012a094ae1',
  name: 'My First Device',
  location: 'undefined',
  status: 'ACTIVE',
  parameters: {},
  capabilities: { door: 'closed' },
  isEmulated: true }
```
