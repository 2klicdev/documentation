var sdk = require('./sdk');

// Longitude, latitude.
var montreal = [-73.5781163, 45.4928131];
var mexicoCity = [-99.2836969, 19.3910038];

function createRootLocation(name, location) {
    return sdk.locations.create({
        name: name,
        geo: location,
    }).then(function (result) {
        return result.data;
    });
}

function createChildLocation(parent, name) {
    return sdk.locations.create({
        parent: parent._id,
        name: name,
        geo: parent.geo,
    }).then(function (result) {
        return result.data;
    });
}

function getLocationTree(root, depth) {
    return sdk.locations.get(root._id, depth).then(function (result) {
        return result.data;
    });
}

function getAllMyLocations() {
    return sdk.locations.list(-1).then(function (result) {
        return result.data;
    });
}

function deleteLocation(location) {
    return sdk.locations.delete(location._id);
}

function printLocationTree(root, depth) {
    if (!depth) {
        depth = 0;
    }

    console.log('*', root._id, '  '.repeat(depth) + '-', root.name);

    if (root.children) {
        var n = root.children.length;
        for (var i = 0; i < n; i++) {
            printLocationTree(root.children[i], depth + 1);
        }
    }
}

var cleanup = true;

function main() {
    return createRootLocation('2KLIC Montreal', montreal).then(function (rootLocation) {
        console.log('root location created', rootLocation);

        return Promise.all([
            createChildLocation(rootLocation, 'Research Lab'),
            createChildLocation(rootLocation, 'Conference Room'),
        ]).then(function (childLocations) {
            console.log('research lab created', childLocations[0]);
            console.log('conference room created', childLocations[1]);
        });
    }).then(function () {
        return createRootLocation('2KLIC Mexico', mexicoCity);
    }).then(function (root) {
        return Promise.all([
            createChildLocation(root, 'Showroom'),
            createChildLocation(root, 'Sales Offices'),
        ]).then(function (children) {
            return createChildLocation(children[1], 'Reception');
        }).then(function () {
            console.log('Mexico Locations (max depth = 1):');
            return getLocationTree(root, 1).then(function (location) {
                printLocationTree(location);
            });
        });
    }).then(function () {
        console.log('All locations:');
        return getAllMyLocations().then(function (locations) {
            var n = locations.length;
            for (var i = 0; i < n; i++) {
                printLocationTree(locations[i]);
                if (cleanup) locations[i] = deleteLocation(locations[i]);
            }
            if (cleanup) return Promise.all(locations);
        });
    });
}

if (require.main === module) {
    main();
}
