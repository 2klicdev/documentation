Logging In
==========

Most useful methods in the API require authentication, so our first
step before doing anything interesting is logging in using the
credentials you registered with.

This is fairly self-explanatory. We instantiate the SDK with the base
URL of the API server, and call its ``authenticate`` method using our
credentials.

.. literalinclude:: 01_login.js
   :language: javascript

When you run this script, you should see output similar to this:

.. literalinclude:: 01_output.js
   :language: javascript

Copy the value of the ``token`` property and save it as your config's
``authToken`` property. (But don't commit it to version control!)

.. tip:: Now is a good time to point out that all SDK methods return
   promises, so it's ready for async/await if you're writing ES7.

   .. literalinclude:: 01_login_es7.js
      :language: javascript
      :lines: 11-21

   We will stick to plain ES5 for the remainder of this tutorial.
