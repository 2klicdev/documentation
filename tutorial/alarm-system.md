# Alarm System

The home alarm system consists primarily of a set of zones that can be
armed or disarmed. A zone monitors a single capability's state. If the
capability value changes from a known good state, the zone is
considered tripped. An armed zone that gets tripped will trigger an
alarm, and that zone will be in an alarm state until it is disarmed,
regardless of whether or not the zone remains tripped.

## Adding a Zone

Let's create a zone using the door contact sensor we created
previously:

```javascript
request({
    method: 'POST',
    url: config.baseUrl + '/alarm/zones',
    json: {
        device: '560ed06a6939c601c50b7438',
        capability: 'door',
        normalState: 'closed'
    }
}, function (error, response, body) {
    console.log(body);
});
```

```javascript
{ _id: '561694036939c6014e06cf18',
  device: '560ed06a6939c601c50b7438',
  capability: 'door',
  normalState: 'closed',
  sensorStatus: 'normal',
  zoneStatus: 'disarmed' }
```

The `sensorStatus` attribute lets us know if the capability is in its
normal state or if it is tripped, and the `zoneStatus` attribute
indicates the current state of the zone. Normally this is `disarmed`
or `armed`. If the zone is armed and gets tripped, the `zoneStatus`
will change to `alarm`.

Now we can run the code from the previous section again (opening and
closing the door), and see what the alarm system reports.

```
connected
opening door
event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'open' },
  type: 'update',
  timestamp: '2015-10-08T16:05:39.007515Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'tripped' },
  type: 'update',
  timestamp: '2015-10-08T16:05:39.007515Z' }
closing door
event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'closed' },
  type: 'update',
  timestamp: '2015-10-08T16:05:39.501881Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'normal' },
  type: 'update',
  timestamp: '2015-10-08T16:05:39.501881Z' }
done
disconnected
```

## Arming and Disarming

Let's arm the system and try that again. To arm the system, we need to
get an arming token by validating a PIN. We'll use the default sandbox
PIN (0000) in this case.

```javascript
function getArmToken(pin, callback) {
    request({
        method: 'POST',
        url: config.baseUrl + '/alarm/access',
        json: { pin: pin }
    }, callback);
}

getArmToken('0000', function (error, response, body) {
    request({
        method: 'PUT',
        url: config.baseUrl + '/alarm?armToken=' + body.armToken,
        json: { status: 'armed' }
    }, function (error, response, body) {
        console.log(body);
    });
});
```

```javascript
{ status: 'armed',
  zones:
   [ { _id: '561694036939c6014e06cf18',
       device: '560ed06a6939c601c50b7438',
       capability: 'door',
       normalState: 'closed',
       sensorStatus: 'normal',
       zoneStatus: 'armed' } ],
  sectors: [] }
```

Now we can try opening the door once more:

```
connected
opening door
event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'open' },
  type: 'update',
  timestamp: '2015-10-08T16:30:12.145527Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'tripped' },
  type: 'update',
  timestamp: '2015-10-08T16:30:12.145527Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { zoneStatus: 'alarm' },
  type: 'update',
  timestamp: '2015-10-08T16:30:12.146392Z' }
closing door
  event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'closed' },
  type: 'update',
  timestamp: '2015-10-08T16:30:12.620805Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'normal' },
  type: 'update',
  timestamp: '2015-10-08T16:30:12.620805Z' }
done
disconnected
```

Note that the `zoneStatus` remains `alarm`. Now lets disarm the
system. We use the same code as we did to arm the system, but change
the system `status` to `disarmed`. The output should look something
like:

```
{ status: 'disarmed',
  zones:
   [ { _id: '561694036939c6014e06cf18',
       device: '560ed06a6939c601c50b7438',
       capability: 'door',
       normalState: 'closed',
       sensorStatus: 'normal',
       zoneStatus: 'disarmed' } ],
  sectors: [] }
```

## Entry and Exit Delays

Zones may be configured with entry or exit delays. An exit delay
defers the arming of a zone until the delay expires. This allows the
user to exit the home after arming the system. An entry delay allows
the user some time to disarm the system after entering the home by
delaying the alarm on a zone after it is tripped.

Let's reconfigure our door with 5-second delays.

```javascript
request({
    method: 'PUT',
    url: config.baseUrl + '/alarm/zones/561694036939c6014e06cf18',
    json: {
        exitDelay: 5,
        entryDelay: 5
    }
}, function (error, response, body) {
    console.log(body);
});
```

Now let's arm the system. This time, we'll subscribe to the WebSocket
so that we can see the zone becoming armed after its exit delay. Then
we'll open the door, wait for the entry delay to expire, see the
alarm, and then disarm the system.

The code, this time from start to finish:
```javascript
var request = require('request');
var WebSocket = require('ws');
var config = require('./config.js');

var ws = new WebSocket('ws://' + config.host + ':' + config.port + config.path + '/events');

ws.on('open', function () {
    console.log('connected');
});

ws.on('message', function (data, flags) {
    var msg = JSON.parse(data);
    console.log('event: ', msg);
});

ws.on('close', function () {
    console.log('disconnected');
});

function getArmToken(pin, callback) {
    request({
        method: 'POST',
        url: config.baseUrl + '/alarm/access',
        json: { pin: pin }
    }, callback);
}

getArmToken('0000', function (error, response, body) {
    request({
        method: 'PUT',
        url: config.baseUrl + '/alarm?armToken=' + body.armToken,
        json: { status: 'armed' }
    }, function (error, response, body) {
        console.log(body);
    });
});

var doorId = '560ed06a6939c601c50b7438';
var doorUrl = config.baseUrl +
    '/protocols/emu/' + doorId +
    '/capabilities/door';

setTimeout(function () {
    console.log('opening door');
    request({
        method: 'PUT',
        url: doorUrl,
        json: { value: 'open' }
    });
}, 7000);

setTimeout(function () {
    console.log('closing door');
    request({
        method: 'PUT',
        url: doorUrl,
        json: { value: 'closed' }
    });
}, 8000);

setTimeout(function () {
    getArmToken('0000', function (error, response, body) {
        request({
            method: 'PUT',
            url: config.baseUrl + '/alarm?armToken=' + body.armToken,
            json: { status: 'disarmed' }
        }, function (error, response, body) {
            console.log(body);
        });
    })
}, 17000);

setTimeout(function () {
    console.log('done');
    ws.close();
}, 20000);
```

And the output. This will take about 20 seconds to run. If you're not
playing along at home, note the timestamps in the following output.

```
connected
event:  { path: '/alarm',
  resource: { status: 'armed', zones: [ [Object] ], sectors: [] },
  type: 'update',
  timestamp: '2015-10-08T16:43:53.849515Z' }
{ status: 'armed',
  zones:
   [ { _id: '561694036939c6014e06cf18',
       device: '560ed06a6939c601c50b7438',
       capability: 'door',
       normalState: 'closed',
       exitDelay: 5,
       entryDelay: 5,
       sensorStatus: 'normal',
       zoneStatus: 'exit_delay',
       delayUntil: '2015-10-08T16:43:58.849370Z' } ],
  sectors: [] }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { zoneStatus: 'armed' },
  type: 'update',
  timestamp: '2015-10-08T16:43:58.850325Z' }
opening door
event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'open' },
  type: 'update',
  timestamp: '2015-10-08T16:44:00.835903Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'tripped' },
  type: 'update',
  timestamp: '2015-10-08T16:44:00.835903Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource:
   { zoneStatus: 'entry_delay',
     delayUntil: '2015-10-08T16:44:05.836096Z' },
  type: 'update',
  timestamp: '2015-10-08T16:44:00.836223Z' }
closing door
event:  { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'closed' },
  type: 'update',
  timestamp: '2015-10-08T16:44:01.834312Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { sensorStatus: 'normal' },
  type: 'update',
  timestamp: '2015-10-08T16:44:01.834312Z' }
event:  { path: '/alarm/zones/561694036939c6014e06cf18',
  resource: { zoneStatus: 'alarm' },
  type: 'update',
  timestamp: '2015-10-08T16:44:05.836254Z' }
event:  { path: '/alarm',
  resource: { status: 'disarmed', zones: [ [Object] ], sectors: [] },
  type: 'update',
  timestamp: '2015-10-08T16:44:10.841046Z' }
{ status: 'disarmed',
  zones:
   [ { _id: '561694036939c6014e06cf18',
       device: '560ed06a6939c601c50b7438',
       capability: 'door',
       normalState: 'closed',
       exitDelay: 5,
       entryDelay: 5,
       sensorStatus: 'normal',
       zoneStatus: 'disarmed' } ],
  sectors: [] }
done
disconnected
```
