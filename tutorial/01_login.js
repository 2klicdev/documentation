var config = require('./config');
var PlatformSdk = require('@2klic/2klic_io-sdk');

var sdk = new PlatformSdk({
    app_key: config.appKey,
    url: config.apiUrl,
    unsafe: config.unsafe,
    apiVersion: '2.0',
});

function main() {
    return sdk.authenticate({
        username: config.username,
        password: config.password,
    }).then(function (user) {
        console.log(user);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
