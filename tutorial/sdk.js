/* sdk.js */
var config = require('./config');
var PlatformSdk = require('@2klic/2klic_io-sdk');

var sdk = new PlatformSdk({
    app_key: config.appKey,
    url: config.apiUrl,
    unsafe: config.unsafe,
    apiVersion: '2.0',

    // Use our login authentication from the login tutorial:
    auth: { token: config.authToken },
});

module.exports = sdk;
