Locations
=========

.. index:: property, location

Locations are user-defined entities that are arranged in trees.  In
user-facing contexts, root locations are referred to as
**properties**.  In this tutorial we will create two location trees,
retrieve them, and delete them.

Preliminaries
-------------

We'll need our :ref:`SDK helper <sdk-helper>`:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 1
   :linenos:
   :lineno-match:

And a couple of latitude/longitude pairs for our locations:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 3-6
   :linenos:
   :lineno-match:

Creating a Location Tree
------------------------

We will define a couple of helper functions for creating locations.
Recall from the :ref:`controller binding tutorial
<controller-binding-result>` that SDK results encode both the HTTP
response status code, and the JSON data from the response body.  Our
helpers will ignore the status code, and simply return the data.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 7-24
   :linenos:
   :lineno-match:

Down in our main function, we create a simple location tree with a
root location and two child locations.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 59-70
   :linenos:
   :lineno-match:

Retrieving and Inspecting a Location Tree
-----------------------------------------

Our locations would not be very useful if we could not retrieve them
later. Let's create a second location tree, with two direct children
and one sub-sub-location, and then retrieve it. We need another helper
function to retrieve the tree to a specific depth:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 26-30
   :linenos:
   :lineno-match:

We also need a recursive helper function to print the location
tree. Note that when retrieved, the child locations are nested inside
of their parent location objects in the ``children`` array.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 42-55
   :linenos:
   :lineno-match:

Here's the code in main:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 70-84
   :linenos:
   :lineno-match:

Retrieving (and Deleting) All Known Locations
---------------------------------------------

It's also possible to retrieve all locations that your user account
has access to.  In this helper function we pass ``-1`` to the list
method, specifying that we want to retrieve the full location trees
with no maximum depth.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 32-36
   :linenos:
   :lineno-match:

While we're here, we can demonstrate location deletion.  If you'd like
to run this tutorial multiple times, it will be helpful to clean up
the locations created by each run.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 38-40
   :linenos:
   :lineno-match:

We'll add a flag variable to be able to easily enable and disable the
cleanup code.

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 57
   :linenos:
   :lineno-match:

Here's the relevant code added to the main function:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 84-95
   :linenos:
   :lineno-match:

Putting it Together
-------------------

Here is our entire main function:

.. literalinclude:: 03_locations.js
   :language: javascript
   :lines: 57-99
   :linenos:
   :lineno-match:

And the output from running our script:

.. literalinclude:: 03_output.txt
   :language: text

The complete source code can be found on `Bitbucket`_.

.. _Bitbucket:
   https://bitbucket.org/2klicdev/documentation/raw/master/tutorial/03_locations.js
