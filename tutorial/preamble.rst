Preamble
========

In this tutorial we will start with an empty test/sandbox controller,
add emulated devices, set up an alarm system and see how it behaves,
and add more users and configure their permissions. The sample code is
written for Node.js using the 2KLIC Platform SDK, so you can
initialize your environment like so:

.. code-block:: bash

   npm install --save '@2klic/2klic_io-sdk'

The current SDK is a thin layer over our RESTful API, so if you are
not targetting Node.js or the browser, it should be trivial to
translate SDK methods into API requests. Refer to our `Swagger
documentation`_ for more information about the API.

.. _Swagger documentation: http://api.2klic.io/v2

The config module will store our local configuration data:

.. literalinclude:: config.js
   :language: javascript

.. caution:: Do not commit private authentication tokens, keys, or
   passwords to version control!
