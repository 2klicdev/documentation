var config = require('./config');
var sdk = require('./sdk');

function main() {
    return sdk.devices.delete(config.switchDevice).then(function (result) {
        console.dir(result);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
