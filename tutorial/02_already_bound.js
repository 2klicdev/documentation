{ [Error: Someone else already added this gateway]
  statusCode: 403,
  statusText: undefined,
  code: 'GATEWAY_ALREADY_ADDED',
  parameters: undefined,
  error: undefined,
  body:
   { statusCode: 403,
     error: 'Forbidden',
     message: 'Someone else already added this gateway',
     code: 'GATEWAY_ALREADY_ADDED' } }
