# Subscribing to Events

The Smart Controller supports publishing events to its clients, so
that they do not have to poll for changes. We will be connecting to
its WebSocket, and then triggering a couple of events so that we can
see what they look like.

## Connecting to the Events WebSocket

```javascript
var ws = new WebSocket('ws://' + config.host + ':' + config.port + config.path + '/events');

ws.on('open', function () {
    console.log('connected');
});

ws.on('message', function (data, flags) {
    var msg = JSON.parse(data);
    console.log('event:', msg);
});

ws.on('close', function () {
    console.log('disconnected');
});
```

## Generating Some Events

We have an emulated device: the door contact we added in the previous
tutorial. Let's see what happens when the door is opened and closed!

You will need the `_id` of the device on your instance.

```javascript
var doorId = '560ed06a6939c601c50b7438';
var doorUrl = config.baseUrl +
    '/protocols/emu/' + doorId +
    '/capabilities/door';

setTimeout(function () {
    console.log('opening door');
    request({
        method: 'PUT',
        url: doorUrl,
        json: { value: 'open' }
    });
}, 500);

setTimeout(function () {
    console.log('closing door');
    request({
        method: 'PUT',
        url: doorUrl,
        json: { value: 'closed' }
    });
}, 1000);

setTimeout(function () {
    console.log('done');
    ws.close();
}, 1500);
```

Putting it all together, you should get output similar to the following.

```
connected
opening door
event: { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'open' },
  type: 'update',
  timestamp: '2015-10-02T19:13:18.949084Z' }
closing door
event: { path: '/devices/560ed06a6939c601c50b7438/capabilities/door',
  resource: { value: 'closed' },
  type: 'update',
  timestamp: '2015-10-02T19:13:19.444160Z' }
done
disconnected
```
