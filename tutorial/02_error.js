{ [Error: This device does not exist]
  statusCode: 404,
  statusText: undefined,
  code: 'device_not_found',
  parameters: undefined,
  error: undefined,
  body:
   { statusCode: 404,
     error: 'Not Found',
     message: 'This device does not exist',
     code: 'device_not_found' } }
