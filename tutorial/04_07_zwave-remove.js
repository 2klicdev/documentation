var config = require('./config');
var sdk = require('./sdk');

function main() {
    return sdk.zwave.exclude(config.gatewayId).then(function (result) {
        console.dir(result);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
