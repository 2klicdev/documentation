/* config.js */

// The API server URL.
exports.apiUrl = 'https://api.2klic.io/v2';

// Replace with your Smart Controller's MAC address.
exports.controllerMac = '00:00:00:00:00:00';

exports.appKey = 'your app key (not available yet)';

exports.unsafe = false;

exports.username = 'the email address you registered with';
exports.password = 'your password';

exports.authToken = 'filled in login tutorial';
exports.gatewayId = 'filled in controller binding tutorial';
exports.switchDevice = 'filled in device tutorial';
