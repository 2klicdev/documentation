Scenarios
=========

.. warning:: This section is a rough draft.  Things may not be
             explained clearly enough yet.

Scenarios define rules for task automation.  They each have a
*trigger* and an associated *action*.  A scenario rule is an object
with the following shape:

.. code-block:: javascript

   {
      trigger: { type: "", ... },
      action: { type: "", ... },
      enabled: true
   }

Scenarios are managed via five API endpoints (currently implemented
only on the Smart Controller, not in the public API):

.. code-block:: javascript

   { method: 'GET', path: '/scenarios', config: Scenario.getAll },
   { method: 'POST', path: '/scenarios', config: Scenario.create },
   { method: 'GET', path: '/scenarios/{id}', config: Scenario.read },
   { method: 'PUT', path: '/scenarios/{id}', config: Scenario.update },
   { method: 'DELETE', path: '/scenarios/{id}', config: Scenario.delete },

Triggers
--------

There are six types of scenario triggers: capability, schedule,
startup, shutdown, alarmSystemStatus, and zoneStatus.

Capability
~~~~~~~~~~

A capability trigger watches for changes in a device capability's
value.  When a condition matches the new value, the scenario is
triggered.  The device and capability are selected by a restricted
condition.  Conditions are described in further detail later in this
document.

.. code-block:: javascript

   {
      type: "capability",
      device: { type: "exact", value: "58138a6adef9f635d0ec5dcc" },
      capability: { type: "exact", value: "airTemperature" },
      condition: { type: "all", conditions: [
          { type: ">", value: 12, unit: "C" },
          { type: "<", value: 28, unit: "C" }
      ] }
   }

Schedule
~~~~~~~~

A schedule trigger runs a recurring action.  The schedules in the
trigger use the same format as those parsed by the `later`_ library.

.. _later: http://bunkat.github.io/later/

.. code-block:: javascript

   var later = require('later');
   var schedule = later.parse.text('every 15 minutes').schedules;
   var scenario = {
      trigger: {
         type: "schedule",
         schedules: schedules
      },
      action: { /* ... */ }
   };

Startup
~~~~~~~

A startup trigger runs every time the Smart Controller starts up:
either because the controller has booted, or because the controller
stopped temporarily for an upgrade.

.. code-block:: javascript

   { type: "startup" }

Shutdown
~~~~~~~~

A shutdown trigger runs every time the Smart Controller shuts down:
either because (for example) the controller is being safely shutdown
before running out of power when running on battery, or before
stopping temporarily to upgrade.

.. code-block:: javascript

   { type: "shutdown" }

Alarm System Status
~~~~~~~~~~~~~~~~~~~

An alarm system status trigger runs when an alarm system's status
matches a specified condition.  The alarm system ID and alarm system
status fields are restricted conditions.

.. code-block:: javascript

   {
      type: "alarmSystemStatus",
      alarmSystem: { type: "exact", value: "58138a6adef9f635d0ec5dd5" },
      alarmSystemStatus: { type: "exact", value: "disarmed" }
   }

The possible alarm system status values are: disarmed, armed,
perimeter, sector.

Zone Status
~~~~~~~~~~~

A zone status trigger runs when a zone's status matches a specified
condition.  The alarm system ID, zone ID, and zone status fields are
restricted conditions.

.. code-block:: javascript

   {
      type: "zoneStatus",
      alarmSystem: { type: "exact", value: "58138a6adef9f635d0ec5dd5" },
      zone: { type: "always" }, // any zone
      zoneStatus: { type: "exact", value: "alarm" }
   }

The possible zone status values are: disarmed, bypassed, exitDelay,
armed, entryDelay, alarm, suspended.

Actions
-------

There is one type of scenario action: set capability.

Set Capability
~~~~~~~~~~~~~~

Sets the value of a device's capability.

.. code-block:: javascript

   {
      type: "setCapability",
      device: "device-id",
      capability: "capability-name",
      value: "the value",
      unit: "unit for number caps (C, F, ...), not needed for enum caps"
   }

Conditions
----------

There are three categories of conditions: compositional conditions
(not, any, all), absolute conditions (always, never), conditions to
compare enum caps (exact), and conditions to compare float caps (<,
<=, >, >=, =).  Restricted conditions allow compositional, absolute,
and exact condition types.

Composing Conditions
~~~~~~~~~~~~~~~~~~~~

*Not* negates another condition.

.. code-block:: javascript

   {
      type: "not",
      condition: {}
   }

*All* and *any* check whether all or any of the sub-conditions match.

.. code-block:: javascript

   {
      type: "all", // all | any
      conditions: []
   }

Absolute Conditions
~~~~~~~~~~~~~~~~~~~

*Always* matches anything.  *Never* matches nothing.

.. code-block:: javascript

   {
      type: "always" // always | never
   }

Exact
~~~~~

*Exact* matches a single enum case.

.. code-block:: javascript

   {
      type: "exact",
      value: "theEnumCase"
   }

Approximate Comparisons
~~~~~~~~~~~~~~~~~~~~~~~

The relational conditions check whether a float value matches within a
given tolerance, which can be optionally specified with an epsilon
value (the default is 10e-8).  The units must be compatible with the
given capability (units that can be converted, like Celsius to/from
Fahrenheit, will be converted before comparing).

.. code-block:: javascript

   {
      type: "<", // < | <= | > | >= | =
      value: 12,
      unit: "C",
      epsilon: 0.2
   }
