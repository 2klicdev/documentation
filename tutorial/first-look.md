# A First Look at the Home

Let's take a look at what our empty home looks like.

```javascript
/* 00_intro.js */

var request = require('request');
var config = require('./config.js');

request({
    uri: config.baseUrl,
    json: {}
}, function (error, response, body) {
    console.log(body);
});
```

Let's try it:

```bash
$ node 00_intro.js
{ _id: '560ec6142ee2a9d04c32e03f',
  mac: 'fc:aa:14:7a:e2:e6',
  home: '560ec6142ee2a9d04c32e03e',
  api: 'https://sandbox.2klic.io/560ec6142ee2a9d00c32e030/api/v1',
  release:
   { name: 'home',
     version: '0.2.4_test',
     variant: 'test',
     isCurrent: true },
  uuid: '456f62fe-6e41-3a59-a984-e800376ee9b3',
  devices: [],
  locations: [],
  protocols: [ 'emu' ],
  alarm: { status: 'disarmed', zones: [], sectors: [] } }
```
