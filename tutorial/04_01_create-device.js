var config = require('./config');
var sdk = require('./sdk');

function main() {
    return sdk.devices.create({
        model: 'zwave_86_103_4b',
        name: 'My First Device',
        // Optional location ID (can be set later):
        //location: locationId,
        gateway: config.gatewayId,
        isEmulated: true,
    }).then(function (result) {
        console.dir(result.data);
    }).catch(function (error) {
        console.error(error);
    });
}

if (require.main === module) {
    main();
}
